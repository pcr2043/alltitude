const path = require('path')
const express = require('express')
const consola = require('consola')

var request = require('request');
const { Nuxt, Builder } = require('nuxt')

const app = express()

// must specify options hash even if no options provided!
const phpExpress = require('php-express')({
  // assumes php is in your PATH
  binPath: 'php'
})





// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')



async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
